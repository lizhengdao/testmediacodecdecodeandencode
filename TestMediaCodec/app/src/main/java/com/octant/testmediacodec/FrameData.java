package com.octant.testmediacodec;

import android.media.MediaFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;

public class FrameData {
    private static FrameData instance=new FrameData();//在自己内部定义自己的一个实例
    private FrameData(){}
    public static FrameData getInstance(){         //此静态方法供外部直接访问
        return instance;
    }

    private ConcurrentLinkedQueue<byte[]> yuvDataQueue = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Long> presentationTimeUsQueue = new ConcurrentLinkedQueue<>();
    private ReentrantLock lock = new ReentrantLock();
    public int QueueNum = 10;
    public int decodeStatus = 0;//0=init,1=start,2=end
    public boolean encode = false;

    public byte[] pollYuvData() {
        try {
            lock.lock();
            if (yuvDataQueue.size()>0){
                return yuvDataQueue.poll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return null;
    }

    public boolean offerYuvData(byte[] yuvData) {
        try {
            lock.lock();
            if (yuvDataQueue.size()<QueueNum){
                yuvDataQueue.offer(yuvData);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return false;
    }

    public long pollPresentationTimeUsQueue() {
        try {
            lock.lock();
            if (presentationTimeUsQueue.size()>0){
                return presentationTimeUsQueue.poll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return 0;
    }

    public boolean offerPresentationTimeUsQueue(long timeUs) {
        try {
            lock.lock();
            if (presentationTimeUsQueue.size()<QueueNum){
                presentationTimeUsQueue.offer(timeUs);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return false;
    }

}
