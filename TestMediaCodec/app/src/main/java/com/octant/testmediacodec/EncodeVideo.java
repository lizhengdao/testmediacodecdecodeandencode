package com.octant.testmediacodec;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.media.Image;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Environment;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 1.configureMediaMuxer，配置MediaMuxer，如果没有现成的mediaFormat，调用createOutputMediaFormat创建一个。
 * 2.configureEncodeCodec，配置MediaCodec
 * 3.EncodeVideo，编码视频帧。注意input的格式是YUV420p或YUV420sp或其它格式，要符合mediaFormat的颜色格式。presentationTimeUs是显示时间，如果没有现成的，调用computePresentationTime创建
 * 4.close,结束时释放资源
 * addTrack要在编码INFO_OUTPUT_FORMAT_CHANGED时
 * 解码时mediaFormat没有bitrate，使用MediaMetadataRetriever获取
 */

public class EncodeVideo {

    private MediaMuxer muxer;
    MediaFormat outputMediaFormat;
    private int videoTrackIndex;
    private MediaCodec outputMediaCodec;

    public void configureMediaMuxer(String outputPath){
        try {
            muxer = new MediaMuxer(outputPath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createOutputMediaFormat(int width, int height, int frameRate, int bitRate){

        MediaFormat mediaFormat = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, width, height);
        mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
        mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, frameRate);
        mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible);
        // 关键帧间隔时间 单位s，设置为0，则没一个都是关键帧
        mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
        mediaFormat.setInteger(MediaFormat.KEY_PROFILE, MediaCodecInfo.CodecProfileLevel.AVCProfileHigh);
        mediaFormat.setInteger(MediaFormat.KEY_LEVEL, MediaCodecInfo.CodecProfileLevel.AVCLevel51);

        outputMediaFormat = mediaFormat;
//        if (muxer != null) {
//            outputMediaFormat = mediaFormat;
//            videoTrackIndex = muxer.addTrack(outputMediaFormat);
//            muxer.start();
//        }
    }

    public void createOutputMediaFormat(){
        VideoInfo videoInfo = VideoInfo.getInstance();
        MediaFormat mediaFormat = MediaFormat.createVideoFormat(videoInfo.mime, videoInfo.width, videoInfo.height);
        mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, videoInfo.bitRate);
        mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, videoInfo.frameRate);
        mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible);
        // 关键帧间隔时间 单位s，设置为0，则没一个都是关键帧
        mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, videoInfo.iFrameInterval);
        mediaFormat.setInteger(MediaFormat.KEY_PROFILE, videoInfo.profile);
        mediaFormat.setInteger(MediaFormat.KEY_LEVEL, videoInfo.level);

        outputMediaFormat = mediaFormat;
    }

    public void configureEncodeCodec(){
        String mime = outputMediaFormat.getString(MediaFormat.KEY_MIME);
        try {
            outputMediaCodec = MediaCodec.createEncoderByType(mime);
        } catch (IOException e) {
            e.printStackTrace();
        }
        outputMediaCodec.configure(outputMediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        outputMediaCodec.start();
    }

    private static final long DEFAULT_TIMEOUT_US = 10000;
    public void encodeVideo(byte[] input,long presentationTimeUs){

        try {
            int inputBufferIndex = outputMediaCodec.dequeueInputBuffer(-1);
            if (inputBufferIndex >= 0) {

                Image image = outputMediaCodec.getInputImage(inputBufferIndex);
                fillImageFromData(image,input,1);
                //String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".jpg";
                //DecodeVideo.compressToJpeg(path,DecodeVideo.getDataFromImage(image,1),image.getCropRect().width(),image.getCropRect().height());
                outputMediaCodec.queueInputBuffer(inputBufferIndex, 0, input.length, presentationTimeUs, 0);
            }

            MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
            int outputBufferIndex = outputMediaCodec.dequeueOutputBuffer(bufferInfo, DEFAULT_TIMEOUT_US);
            while (outputBufferIndex >= 0) {
                ByteBuffer outputBuffer = outputMediaCodec.getOutputBuffer(outputBufferIndex);

                muxer.writeSampleData(videoTrackIndex, outputBuffer, bufferInfo);

                outputMediaCodec.releaseOutputBuffer(outputBufferIndex, false);

                outputBufferIndex = outputMediaCodec.dequeueOutputBuffer(bufferInfo, DEFAULT_TIMEOUT_US);
            }
            if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED){
                MediaFormat mediaFormat = outputMediaCodec.getOutputFormat();
                videoTrackIndex = muxer.addTrack(mediaFormat);
                muxer.start();

            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Generates the presentation time for frame N, in microseconds.
     */
    public long computePresentationTime(long frameIndex,int fps) {
        return 132 + frameIndex * 1000000 / fps;
    }

    public void close() {
        try {
            muxer.stop();
            muxer.release();
            outputMediaCodec.stop();
            outputMediaCodec.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillImageFromData(Image image,byte[] data, int colorFormat) {
        boolean VERBOSE = false;
        int COLOR_FormatI420 = 0,COLOR_FormatNV21=1;
        String TAG = "getDataFromImage";

        if (colorFormat != COLOR_FormatI420 && colorFormat != COLOR_FormatNV21) {
            throw new IllegalArgumentException("only support COLOR_FormatI420 " + "and COLOR_FormatNV21");
        }
        if (!isImageFormatSupported(image)) {
            throw new RuntimeException("can't convert Image to byte array, format " + image.getFormat());
        }
        Rect crop = image.getCropRect();
        int width = crop.width();
        int height = crop.height();
        Image.Plane[] planes = image.getPlanes();
        byte[] rowData = new byte[planes[0].getRowStride()];
        if (VERBOSE) Log.v(TAG, "fill data to " + planes.length + " planes");
        int channelOffset = 0;
        int outputStride = 1;
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    channelOffset = 0;
                    outputStride = 1;
                    break;
                case 1:
                    if (colorFormat == COLOR_FormatI420) {
                        channelOffset = width * height;
                        outputStride = 1;
                    } else if (colorFormat == COLOR_FormatNV21) {
                        channelOffset = width * height + 1;
                        outputStride = 2;
                    }
                    break;
                case 2:
                    if (colorFormat == COLOR_FormatI420) {
                        channelOffset = (int) (width * height * 1.25);
                        outputStride = 1;
                    } else if (colorFormat == COLOR_FormatNV21) {
                        channelOffset = width * height;
                        outputStride = 2;
                    }
                    break;
            }
            ByteBuffer buffer = planes[i].getBuffer();
            int rowStride = planes[i].getRowStride();
            int pixelStride = planes[i].getPixelStride();
            if (VERBOSE) {
                Log.v(TAG, "pixelStride " + pixelStride);
                Log.v(TAG, "rowStride " + rowStride);
                Log.v(TAG, "width " + width);
                Log.v(TAG, "height " + height);
                Log.v(TAG, "buffer size " + buffer.remaining());
            }
            int shift = (i == 0) ? 0 : 1;
            int w = width >> shift;
            int h = height >> shift;
            buffer.position(rowStride * (crop.top >> shift) + pixelStride * (crop.left >> shift));
            for (int row = 0; row < h; row++) {
                int length;
                if (pixelStride == 1 && outputStride == 1) {
                    length = w;
                    buffer.put(data, channelOffset, length);
                    channelOffset += length;
                } else {
                    length = (w - 1) * pixelStride + 1;
                    /**
                     * 如果pixelStride=2，planes[1]是UVUVUV，planes[2]是VUVUVU
                     * 解码时，plane[1]取0、2、4是U，planes[2]取0、2、4是V
                     * 但是编码时，plane[1]的1、3、5和planes[2]的1、3、5也要赋值，否则出来的颜色不对
                     * 测试发现编码时系统只取了planes[1]，planes[2]不赋值也没影响
                     */
                    int moveIndex = 0;
                    if (colorFormat == COLOR_FormatI420) {
                        if (channelOffset<length*width*1.25){
                            moveIndex = (int)(length*width*0.25);
                        }else {
                            moveIndex = - (int)(length*width*0.25);
                        }
                    } else if (colorFormat == COLOR_FormatNV21) {
                        if (i==2){
                            moveIndex = 1;
                        }else {
                            moveIndex = -1;
                        }
                    }
                    for (int col = 0; col < w; col++) {
                        rowData[col * pixelStride] = data[channelOffset];
                        rowData[col * pixelStride+1] = data[channelOffset+moveIndex];
                        channelOffset += outputStride;
                    }
                    buffer.put(rowData, 0, length);
                }
                if (row < h - 1) {
                    buffer.position(buffer.position() + rowStride - length);
                }
            }
            if (VERBOSE) Log.v(TAG, "Finished reading data from plane " + i);
        }
    }

    private boolean isImageFormatSupported(Image image) {
        int format = image.getFormat();
        switch (format) {
            case ImageFormat.YUV_420_888:
            case ImageFormat.NV21:
            case ImageFormat.YV12:
                return true;
        }
        return false;
    }
}
