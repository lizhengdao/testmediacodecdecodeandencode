package com.octant.testmediacodec;

import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;

public class VideoInfo {

    private static VideoInfo instance=new VideoInfo();//在自己内部定义自己的一个实例
    private VideoInfo(){}
    public static VideoInfo getInstance(){         //此静态方法供外部直接访问
        return instance;
    }

    String mime = "video/avc";
    int width = 1920;
    int height = 1080;
    int frameRate = 25;
    int bitRate = 8000000;
    int iFrameInterval = 1;
    int profile =  MediaCodecInfo.CodecProfileLevel.AVCProfileHigh;
    int level =  MediaCodecInfo.CodecProfileLevel.AVCLevel51;

    public void initFromMediaMetadataRetriever(MediaMetadataRetriever mmr){
        width = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        height = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        bitRate = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE));
    }

    public void initFromMediaFormat(MediaFormat mf){
        if (mf.containsKey(MediaFormat.KEY_MIME)){
            mime = mf.getString(MediaFormat.KEY_MIME);
        }
        if (mf.containsKey(MediaFormat.KEY_WIDTH)){
            width = mf.getInteger(MediaFormat.KEY_WIDTH);
        }
        if (mf.containsKey(MediaFormat.KEY_HEIGHT)){
            height = mf.getInteger(MediaFormat.KEY_HEIGHT);
        }
        if (mf.containsKey(MediaFormat.KEY_FRAME_RATE)){
            frameRate = mf.getInteger(MediaFormat.KEY_FRAME_RATE);
        }
        if (mf.containsKey(MediaFormat.KEY_BIT_RATE)){
            bitRate = mf.getInteger(MediaFormat.KEY_BIT_RATE);
        }
        if (mf.containsKey(MediaFormat.KEY_I_FRAME_INTERVAL)){
            iFrameInterval = mf.getInteger(MediaFormat.KEY_I_FRAME_INTERVAL);
        }
        if (mf.containsKey(MediaFormat.KEY_PROFILE)){
            profile = mf.getInteger(MediaFormat.KEY_PROFILE);
        }
        if (mf.containsKey(MediaFormat.KEY_LEVEL)){
            level = mf.getInteger(MediaFormat.KEY_LEVEL);
        }
    }

    @Override
    public String toString() {
        return "VideoInfo{" +
                "mime='" + mime + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", frameRate=" + frameRate +
                ", bitRate=" + bitRate +
                ", iFrameInterval=" + iFrameInterval +
                ", profile=" + profile +
                ", level=" + level +
                '}';
    }
}
